wmcube (1.0.2-5) unstable; urgency=medium

  * d/control
    - bump Standards-Version to 4.7.0
    - update my e-mail address
  * d/patches
    - refresh patches
    - add patch to remove pre-ANSI function declaration (Closes: #1098102)

 -- Jeremy Sowden <azazel@debian.org>  Thu, 27 Feb 2025 20:05:21 +0000

wmcube (1.0.2-4) unstable; urgency=medium

  [ Debian Janitor ]
  * debian/copyright
    - Use spaces rather than tabs to start continuation lines.

  [ Doug Torrance ]
  * debian/control
    - Update Maintainer email address to use tracker.d.o.
    - Bump Standards-Version to 4.6.0.
  * debian/copyright
    - Update my copyright years.
  * debian/menu
    - Remove deprecated Debian menu file.
  * debian/patches/*
    - Add Forwarded fields.
  * debian/salsa-ci.yml
    - Add Salsa pipeline config file.
  * debian/upstream/metadata
    - Add DEP-12 upstream metadata file.

 -- Doug Torrance <dtorrance@piedmont.edu>  Sun, 24 Oct 2021 20:01:09 -0400

wmcube (1.0.2-3) unstable; urgency=medium

  * Update d/watch.
  * Bump compat to 13.
  * Update upstream details.

 -- Jeremy Sowden <jeremy@azazel.net>  Mon, 25 May 2020 14:02:32 +0100

wmcube (1.0.2-2) unstable; urgency=medium

  [ Doug Torrance ]
  * Update Vcs-* after migration to Salsa.
  * Update Format in d/copyright to https.

  [ Jeremy Sowden ]
  * Bump compat to 12 and replaced d/compat with b-d on debhelper-compat.
  * Add myself to uploaders.
  * Bump Standards-Version to 4.5.0.
  * Add patch to use wmgeneral.* from libdockapp. Fixes FTBFS with Gcc-10.
    Closes: #957942
  * Remove get-orig-source.
  * Remove copies of doc's /usr/share/wmcube.
  * Add patch to remove root chown from Makefile.
  * Set Rules-Requires-Root: no.

 -- Jeremy Sowden <jeremy@azazel.net>  Sun, 03 May 2020 16:22:22 +0100

wmcube (1.0.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/control
    - Bump Standards-Version to 3.9.7.
    - Use https protocol for Vcs-Browser.
  * debian/patches
    - Remove directory; patches applied upstream.
  * debian/rules
    - Enable all hardening flags.

 -- Doug Torrance <dtorrance@piedmont.edu>  Fri, 18 Mar 2016 22:15:18 -0400

wmcube (1.0.1-3) unstable; urgency=medium

  * debian/control
    - Update Vcs-* fields.
    - Set Debian Window Maker Team as Maintainer; move myself to
      Uploaders with updated email address.
  * debian/{copyright,patches}
    - Update email address.

 -- Doug Torrance <dtorrance@piedmont.edu>  Mon, 24 Aug 2015 23:18:48 -0400

wmcube (1.0.1-2) unstable; urgency=medium

  * debian/patches/remove_inline_keywords.patch
    - Allow builds using gcc5 (Closes: #790293).

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Wed, 15 Jul 2015 12:51:57 -0600

wmcube (1.0.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/{install,wmc/*}
    - Remove files; *.wmc added upstream.
  * debian/{manpages,wmcube.1}
    - Remove files; manpage added upstream.
  * debian/patches
    - Remove previous patches; applied upstream.
  * debian/patches/manpage_escape_hyphens.patch
    - New patch; avoid hyphen-used-as-minus-sign Lintian warning.
  * debian/rules
    - Update override_dh_auto_install target to set installation
      directories.  (These settings were originally in
      debian/patches/update_makefile.patch, which upstream applied
      in part.  Since these settings are more Debian-specific, I'm
      moving this to debian/rules.)

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Thu, 19 Feb 2015 20:40:57 -0600

wmcube (1.0.0-1) unstable; urgency=medium

  * New upstream release.
  * New maintainer (Closes: #775110).
  * debian/compat
    - Bump to 9.
  * debian/control
    - Bump versioned dependency on debhelper to >=9.
    - Remove dpatch from Build-Depends.
    - Bump Standards-Version to 3.9.6.
    - Update Homepage.
    - Add Vcs-*.
    - Add ${misc:Depends} to Depends.
    - Remove duplicate Section and Priority fields.
  * debian/copyright
    - Update to DEP5 machine-readable format version 1.0.
  * debian/install
    - New file; install extra *.wmc files.
  * debian/manpages
    - New file.
  * debian/patches
    - (10_fixes_for_0.99pre1_upgrade.dpatch, 20_parallel_build.dpatch,
      50_bts-504844_fix_ftbfs_gcc4.4.dpatch) Remove patches; no longer
      needed.
    - (30_bts-386850_fix_smp.patch, 40_bts-357072_long_uptime_fix.patch)
      Convert from dpatch to quilt, change extension from .dpatch to
      .patch, update for new upstream version, and add DEP3 header.
    - (update_makefile.patch) Various changes to upstream Makefile to
      properly build package.
    - (fix_-Wunused-result.patch) Fix compiler warnings.
    - (freebsd_sysctl.patch): Use sysctl instead of kvm to get cpu stats
      on kfreebsd.
    - (fix_grammar.patch) Fix grammar in error message.
    - (fix_cppcheck_warnings.patch) Fix warnings found by cppcheck.
  * debian/README.source
    - Remove out-of-date file.
  * debian/rules
    - Add get-orig-source target.
    - Update to use dh.
    - Add override_dh_auto_build target to pass CFLAGS from
      dpkg-buildflags and set FREEBSD macro when building on kfreebsd.
    - Add override_dh_auto_install target to remove doc files installed
      by upstream that cause Lintian warnings.
  * debian/source/format
    - New file; use 3.0 (quilt).
  * debian/watch
    - Update with new location.
  * debian/wmcube.1
    - Update with descriptions of options.

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Wed, 14 Jan 2015 20:27:37 -0600

wmcube (0.99-pre1-3) unstable; urgency=low

  * QA upload.
  * debian/control
    - Set QA Group as maintainer.
    - Bump Standards-Version to 3.8.0.
      + Add debian/README.source.
  * debian/patches/50_bts-504844_fix_ftbfs_gcc4.4.dpatch
    - Add to fix a FTBFS using GCC 4.4; thanks to Martin Michlmayr for the
      report and the patch (Closes: #504844).

 -- Sandro Tosi <morph@debian.org>  Sat, 08 Nov 2008 11:40:47 +0100

wmcube (0.99-pre1-2) unstable; urgency=low

  * debian/wmc/beryllium.wmc
    - Add new wmc (Closes: #466606).

 -- Sandro Tosi <matrixhasu@gmail.com>  Sat, 08 Mar 2008 16:24:38 +0100

wmcube (0.99-pre1-1) unstable; urgency=low

  * New upstream release (Closes: #239522).
  * debian/control
    - Bump Standards-Version to 3.7.3.
    - Add Homepage field.
  * debian/menu
    - Update to new menu policy.
  * debian/copyright
    - Indent copyright and upstream author with 4 spaces.
    - Update copyright note.
    - Add copyright/license notes for wmapp directory.
  * debian/README.debian
    - Remove package description
    - Update upstream email & website address.
  * debian/watch
    - Add.
  * debian/rules
    - Install upstream changelog.
    - Don't install removed files.
    - Change binary location in install target.
    - Remove TODO from docs installation since no longer in upstream
      tarball.
    - Integrate destination dir creation into install target.
    - Remove execution rights from data files (*.wmc).
    - Clean target reorg to be run on patched source.
    - Build target depends on patch, not build-stamp on patch-stamp.
  * debian/3dObjects/
    - Rename to debian/wmc to match upstream directory
  * debian/patches/10_fixes_for_0.99pre1_upgrade.dpatch
    - Add to let new upstream release compile; thanks to Peter De Wachter
      from #debian-mentors IRC channel.
  * debian/patches/10_datapath_old.dpatch
    - Remove since merged upstream / no longer applicable.
  * debian/patches/30_restore_pristine_code.dpatch
    - Remove since merged upstream / no longer applicable.
  * debian/dirs
    - Remove.
  * debian/patches/20_secfix_old.dpatch
    - Remove since no longer applicable.
  * debian/patches/20_parallel_build.dpatch
    - Allow parallel build.
  * debian/patches/30_bts-386850_fix_smp.dpatch
    - Fix smp support (Closes: 386850).
  * debian/patches/40_bts-357072_long_uptime_fix.dpatch
    - Fix program running on machine with long uptime (Closes: #357072).
  * debian/wmcube.1
    - Adapt to new upstream release.

 -- Sandro Tosi <matrixhasu@gmail.com>  Wed, 30 Jan 2008 19:58:58 +0100

wmcube (0.98-8) unstable; urgency=low

  * New maintainer (Closes: #414531).
  * debian/control
    - Add myself as new maintainer.
    - Little reformat of short and long descriptions.
    - Add dependency against dpatch.
  * debian/copyright
    - Set myself as new maintainer.
    - Add copyright notes for wmgeneral files.
    - Reformat license note.
  * debian/docs
    - Remove since integrated into debian/rules.
  * debian/rules
    - Change dh_installdocs call adding filenames as parameters..
    - Remove some commented lines here and there.
    - Manpage installation done using dh_installman.
    - Added install target.
    - Move 3dObjects/{README,CONTRIBUTE} from install to dh_installdocs
    - Remove "source diff" targe" target.
    - Remove custom patch application code since we are now using
      dpatch.
    - Patches to apply are selected using debian/patches/00list and not a
      variable.
    - Fix installation of 3dObjects files.
    - Fix a lintian warning on "make clean" call.
  * debian/menu
    - Change section to Applications.
  * debian/post{inst,rm}.debhelper
    - Remove since menu updatation is done automatically now.
  * debian/patches/{10_datapath_old,20_secfix_old}.dpatch
    - Replace old dpatch-style patches.
  * debian/patches/smpfix.dpatch
    - Remove since already present in 20_secfix_old.dpatch.
  * debian/3dObjects/X-{lines,planes}.wmc
    - Move from top level directory into debian since they are not present
      in upstream release.

 -- Sandro Tosi <matrixhasu@gmail.com>  Sun, 07 Oct 2007 16:08:46 +0200

wmcube (0.98-7) unstable; urgency=low

  * QA upload.
  * Set maintainer address to QA group.
  * Fix broken copyright file.
  * Bumped standards version, no changes needed.

 -- Nico Golde <nion@debian.org>  Fri, 23 Mar 2007 20:38:45 +0100

wmcube (0.98-6.1) unstable; urgency=low

  * Non-maintainer upload.
  * Split xlibs-dev build depends into individual Xorg libraries
    (Closes: #346969).
  * Make package lintian clean:
    - Use debhelper v5.
    - Update Standards version to 3.6.2.1.
    - Refer to common license text in debian/copyright.

 -- Bastian Kleineidam <calvin@debian.org>  Fri, 13 Jan 2006 21:13:58 +0100

wmcube (0.98-6) unstable; urgency=low

  * Add two new objects (X-lines & X-planes). Thanks Mathias Czapla.
  * Update Standards-Version:; support noopt and nostrip build options.
  * Update upstream URL in debian/copyright.

 -- Filip Van Raemdonck <mechanix@debian.org>  Thu, 26 Dec 2002 22:13:26 +0100

wmcube (0.98-5) unstable; urgency=high

  * Fix smp support which broke due to the source fixes in previous upload
    (Closes: #125902).
  * Keep urgency at high to try to get this into testing asap to fix the
    security issues.

 -- Filip Van Raemdonck <mechanix@debian.org>  Thu, 20 Dec 2001 19:29:44 +0100

wmcube (0.98-4) unstable; urgency=high

  * Apply security fixes from corecode wmcube-gdk advisory.
  * Adopt dpatch system to apply patches to upstream source during the build.
  * Update Standards-Version:; no changes were necessary.

 -- Filip Van Raemdonck <mechanix@debian.org>  Wed, 19 Dec 2001 10:49:14 +0100

wmcube (0.98-3) unstable; urgency=low

  * Add manpage contributed by Evelyn Mitchell (Closes: #93479).

 -- Filip Van Raemdonck <mechanix@debian.org>  Sat, 28 Jul 2001 19:34:31 +0200

wmcube (0.98-2) unstable; urgency=low

  * Fix incorrect menu entry (Closes: #103450).

 -- Filip Van Raemdonck <mechanix@debian.org>  Fri,  6 Jul 2001 17:20:20 +0200

wmcube (0.98-1) unstable; urgency=low

  * New maintainer (Closes: #98249).
  * New upstream release.
  * Add Build-Depends.
  * Update Standards-Version.
  * Remove post{inst,rm}. They are generated by debhelper.
  * Make wmcube by default look in /usr/share/wmcube for object definitions.

 -- Filip Van Raemdonck <mechanix@debian.org>  Sun, 27 May 2001 01:02:38 +0200

wmcube (0.90-2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix ownership of package.

 -- Sean 'Shaleh' Perry <shaleh@debian.org>  Fri,  6 Oct 2000 10:58:45 -0700

wmcube (0.90-1) unstable; urgency=low

  * Initial release.

 -- Edward C. Lang <edlang@debian.org>  Fri, 14 Apr 2000 17:36:34 +1000
